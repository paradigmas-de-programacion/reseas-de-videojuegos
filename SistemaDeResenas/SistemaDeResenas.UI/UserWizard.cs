﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;
using SistemaDeResenas.BE;

namespace SistemaDeResenas.UI
{
    public partial class UserWizard : Form
    {
        Users userForm;
        BE.User user;

        bool edition;
        bool isDataSaved = false;
        string closeModalString;

        public UserWizard(bool edition, BE.User user)
        {
            InitializeComponent();
            this.edition = edition;
            this.user = user;
        }

        private void UserWizard_Load(object sender, EventArgs e)
        {
            userForm = (Users)Owner.ActiveMdiChild;
            SetWizardMode();
        }
        private void SetWizardMode()
        {
            if (!edition)
            {
                Text = "Crear Usuario";
                button_Save.Text = "Crear";
                textBox_FirstName.Text = string.Empty;
                textBox_LastName.Text = string.Empty;
                dateTimePicker_Birthdate.Value = DateTime.Now;
                textBox_Nickname.Text = string.Empty;
                closeModalString = "¿Desea descartar la creacion de usuario?";
                button_Save.Click += button_Save_ClickCREATE;
            }
            else
            {
                Text = "Editar Usuario";
                button_Save.Text = "Guardar";
                textBox_FirstName.Text = user.FirstNm;
                textBox_LastName.Text = user.LastNm;
                dateTimePicker_Birthdate.Value = user.Birthdate;
                textBox_Nickname.Text = user.Nickname;
                closeModalString = "¿Desea descartar los cambios?";
                button_Save.Click += button_Save_ClickEDIT;
            }
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void UserWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isDataSaved)
            {
                DialogResult result = MessageBox.Show(closeModalString, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No) { e.Cancel = true; }
            }
            userForm.UpdateGrid();
        }

        private void button_Save_ClickCREATE(object sender, EventArgs e)
        {
            try
            {
                if (textBox_FirstName.Text == "") { throw new Exception("Ingrese un nombre"); }
                if (textBox_LastName.Text == "") { throw new Exception("Ingrese un apellido"); }
                if (dateTimePicker_Birthdate.Value == null) { throw new Exception("Ingrese una fecha de nacimiento válida"); }
                if (textBox_Nickname.Text == "") { throw new Exception("Ingrese un alias"); }
                user = new BE.User(0, textBox_FirstName.Text, textBox_LastName.Text, dateTimePicker_Birthdate.Value.ToString(), textBox_Nickname.Text);
                userForm.UserCreate(user);
                isDataSaved = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void button_Save_ClickEDIT(object sender, EventArgs e)
        {
            try
            {
                if (textBox_FirstName.Text == "") { throw new Exception("Ingrese un nombre"); }
                if (textBox_LastName.Text == "") { throw new Exception("Ingrese un apellido"); }
                if (dateTimePicker_Birthdate.Value == null) { throw new Exception("Ingrese una fecha de nacimiento válida"); }
                if (textBox_Nickname.Text == "") { throw new Exception("Ingrese un alias"); }
                user = new BE.User(user.Id,textBox_FirstName.Text, textBox_LastName.Text, dateTimePicker_Birthdate.Value.ToString(), textBox_Nickname.Text);
                userForm.UserModify(user);
                isDataSaved = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}
