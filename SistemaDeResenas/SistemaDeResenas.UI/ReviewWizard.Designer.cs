﻿namespace SistemaDeResenas.UI
{
    partial class ReviewWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Save = new System.Windows.Forms.Button();
            this.textBox_Comment = new System.Windows.Forms.TextBox();
            this.label_Message = new System.Windows.Forms.Label();
            this.label_Videogame = new System.Windows.Forms.Label();
            this.comboBox_Videogames = new System.Windows.Forms.ComboBox();
            this.label_Score = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.panel_Radiobuttons = new System.Windows.Forms.Panel();
            this.panel_Radiobuttons.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(130, 238);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancelar";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(211, 238);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Save.TabIndex = 4;
            this.button_Save.Text = "Crear";
            this.button_Save.UseVisualStyleBackColor = true;
            // 
            // textBox_Comment
            // 
            this.textBox_Comment.Location = new System.Drawing.Point(12, 107);
            this.textBox_Comment.MaxLength = 500;
            this.textBox_Comment.Multiline = true;
            this.textBox_Comment.Name = "textBox_Comment";
            this.textBox_Comment.Size = new System.Drawing.Size(274, 125);
            this.textBox_Comment.TabIndex = 2;
            // 
            // label_Message
            // 
            this.label_Message.AutoSize = true;
            this.label_Message.Location = new System.Drawing.Point(12, 91);
            this.label_Message.Name = "label_Message";
            this.label_Message.Size = new System.Drawing.Size(60, 13);
            this.label_Message.TabIndex = 0;
            this.label_Message.Text = "Comentario";
            // 
            // label_Videogame
            // 
            this.label_Videogame.AutoSize = true;
            this.label_Videogame.Location = new System.Drawing.Point(12, 9);
            this.label_Videogame.Name = "label_Videogame";
            this.label_Videogame.Size = new System.Drawing.Size(36, 13);
            this.label_Videogame.TabIndex = 0;
            this.label_Videogame.Text = "Juego";
            // 
            // comboBox_Videogames
            // 
            this.comboBox_Videogames.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox_Videogames.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_Videogames.FormattingEnabled = true;
            this.comboBox_Videogames.Location = new System.Drawing.Point(12, 25);
            this.comboBox_Videogames.Name = "comboBox_Videogames";
            this.comboBox_Videogames.Size = new System.Drawing.Size(274, 21);
            this.comboBox_Videogames.TabIndex = 0;
            this.comboBox_Videogames.SelectedIndexChanged += new System.EventHandler(this.comboBox_Videogames_SelectedIndexChanged);
            // 
            // label_Score
            // 
            this.label_Score.AutoSize = true;
            this.label_Score.Location = new System.Drawing.Point(12, 49);
            this.label_Score.Name = "label_Score";
            this.label_Score.Size = new System.Drawing.Size(61, 13);
            this.label_Score.TabIndex = 13;
            this.label_Score.Text = "Puntuacion";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(29, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(31, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "1";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.radioButton_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(75, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(31, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "2";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Click += new System.EventHandler(this.radioButton_Click);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(121, 3);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(31, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "3";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Click += new System.EventHandler(this.radioButton_Click);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(167, 3);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(31, 17);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "4";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.Click += new System.EventHandler(this.radioButton_Click);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(213, 3);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(31, 17);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "5";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.Click += new System.EventHandler(this.radioButton_Click);
            // 
            // panel_Radiobuttons
            // 
            this.panel_Radiobuttons.Controls.Add(this.radioButton3);
            this.panel_Radiobuttons.Controls.Add(this.radioButton1);
            this.panel_Radiobuttons.Controls.Add(this.radioButton5);
            this.panel_Radiobuttons.Controls.Add(this.radioButton2);
            this.panel_Radiobuttons.Controls.Add(this.radioButton4);
            this.panel_Radiobuttons.Location = new System.Drawing.Point(12, 66);
            this.panel_Radiobuttons.Name = "panel_Radiobuttons";
            this.panel_Radiobuttons.Size = new System.Drawing.Size(274, 22);
            this.panel_Radiobuttons.TabIndex = 1;
            this.panel_Radiobuttons.TabStop = true;
            // 
            // ReviewWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 271);
            this.Controls.Add(this.panel_Radiobuttons);
            this.Controls.Add(this.label_Score);
            this.Controls.Add(this.comboBox_Videogames);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.textBox_Comment);
            this.Controls.Add(this.label_Message);
            this.Controls.Add(this.label_Videogame);
            this.Name = "ReviewWizard";
            this.Text = "Nueva Reseña";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReviewWizard_FormClosing);
            this.Load += new System.EventHandler(this.Reviews_Load);
            this.panel_Radiobuttons.ResumeLayout(false);
            this.panel_Radiobuttons.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.TextBox textBox_Comment;
        private System.Windows.Forms.Label label_Message;
        private System.Windows.Forms.Label label_Videogame;
        private System.Windows.Forms.ComboBox comboBox_Videogames;
        private System.Windows.Forms.Label label_Score;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Panel panel_Radiobuttons;
    }
}