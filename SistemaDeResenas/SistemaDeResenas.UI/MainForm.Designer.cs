﻿namespace SistemaDeResenas.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip_MainMenu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_Users = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel_Videogames = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip_MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip_MainMenu
            // 
            this.toolStrip_MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_Users,
            this.toolStripLabel_Videogames});
            this.toolStrip_MainMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MainMenu.Name = "toolStrip_MainMenu";
            this.toolStrip_MainMenu.Size = new System.Drawing.Size(800, 25);
            this.toolStrip_MainMenu.TabIndex = 2;
            this.toolStrip_MainMenu.Text = "toolStrip1";
            // 
            // toolStripLabel_Users
            // 
            this.toolStripLabel_Users.Name = "toolStripLabel_Users";
            this.toolStripLabel_Users.Size = new System.Drawing.Size(52, 22);
            this.toolStripLabel_Users.Text = "Usuarios";
            this.toolStripLabel_Users.Click += new System.EventHandler(this.toolStripLabel_Users_Click);
            // 
            // toolStripLabel_Videogames
            // 
            this.toolStripLabel_Videogames.Name = "toolStripLabel_Videogames";
            this.toolStripLabel_Videogames.Size = new System.Drawing.Size(72, 22);
            this.toolStripLabel_Videogames.Text = "Videojuegos";
            this.toolStripLabel_Videogames.Click += new System.EventHandler(this.toolStripLabel_Videogames_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip_MainMenu);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Text = "Sistema de Reseñas de Videojuegos";
            this.toolStrip_MainMenu.ResumeLayout(false);
            this.toolStrip_MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_MainMenu;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Users;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Videogames;
    }
}

