﻿namespace SistemaDeResenas.UI
{
    partial class Videogames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_Videogames = new System.Windows.Forms.DataGridView();
            this.button_newGame = new System.Windows.Forms.Button();
            this.bindingSource_Videogames = new System.Windows.Forms.BindingSource(this.components);
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReviewsAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AverageScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Videogames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_Videogames)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Videogames
            // 
            this.dataGridView_Videogames.AllowUserToAddRows = false;
            this.dataGridView_Videogames.AllowUserToDeleteRows = false;
            this.dataGridView_Videogames.AllowUserToResizeColumns = false;
            this.dataGridView_Videogames.AllowUserToResizeRows = false;
            this.dataGridView_Videogames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Videogames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Videogames.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.ReviewsAmount,
            this.AverageScore,
            this.Edit,
            this.Delete});
            this.dataGridView_Videogames.Location = new System.Drawing.Point(12, 41);
            this.dataGridView_Videogames.MultiSelect = false;
            this.dataGridView_Videogames.Name = "dataGridView_Videogames";
            this.dataGridView_Videogames.Size = new System.Drawing.Size(543, 264);
            this.dataGridView_Videogames.StandardTab = true;
            this.dataGridView_Videogames.TabIndex = 1;
            this.dataGridView_Videogames.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Videogames_CellContentClick);
            // 
            // button_newGame
            // 
            this.button_newGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_newGame.AutoSize = true;
            this.button_newGame.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button_newGame.Location = new System.Drawing.Point(465, 12);
            this.button_newGame.Name = "button_newGame";
            this.button_newGame.Size = new System.Drawing.Size(90, 23);
            this.button_newGame.TabIndex = 0;
            this.button_newGame.Text = "+ Nuevo Juego";
            this.button_newGame.UseVisualStyleBackColor = true;
            this.button_newGame.Click += new System.EventHandler(this.button_newGame_Click);
            // 
            // Title
            // 
            this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Title.HeaderText = "Titulo";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Title.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ReviewsAmount
            // 
            this.ReviewsAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ReviewsAmount.HeaderText = "Cantidad de Reseñas";
            this.ReviewsAmount.Name = "ReviewsAmount";
            this.ReviewsAmount.ReadOnly = true;
            this.ReviewsAmount.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ReviewsAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReviewsAmount.Width = 104;
            // 
            // AverageScore
            // 
            this.AverageScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AverageScore.HeaderText = "Puntuación Promedio";
            this.AverageScore.Name = "AverageScore";
            this.AverageScore.ReadOnly = true;
            this.AverageScore.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AverageScore.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AverageScore.Width = 103;
            // 
            // Edit
            // 
            this.Edit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Edit.HeaderText = "";
            this.Edit.Name = "Edit";
            this.Edit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Edit.Width = 5;
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Delete.HeaderText = "";
            this.Delete.Name = "Delete";
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Delete.Width = 5;
            // 
            // Videogames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 317);
            this.Controls.Add(this.button_newGame);
            this.Controls.Add(this.dataGridView_Videogames);
            this.Name = "Videogames";
            this.Text = "Videojuegos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Videogames_FormClosed);
            this.Load += new System.EventHandler(this.Videogames_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Videogames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_Videogames)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Videogames;
        private System.Windows.Forms.Button button_newGame;
        private System.Windows.Forms.BindingSource bindingSource_Videogames;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReviewsAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn AverageScore;
        private System.Windows.Forms.DataGridViewButtonColumn Edit;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
    }
}