﻿using SistemaDeResenas.BE;
using SistemaDeResenas.BLL;
using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace SistemaDeResenas.UI
{
    public partial class Videogames : Form
    {
        ToolStripLabel label;
        GamesManager gamesManager;

        public Videogames(ToolStripLabel label)
        {
            InitializeComponent();
            this.label = label;
            label.Enabled = false;

            gamesManager = new GamesManager();
        }

        private void Videogames_Load(object sender, EventArgs e)
        {
            UpdateGrid();
        }
        private void Videogames_FormClosed(object sender, FormClosedEventArgs e)
        {
            label.Enabled = true;
        }

        private void UpdateGrid()
        {
            gamesManager.GamesList();
            dataGridView_Videogames.Rows.Clear();
            foreach (Game g in gamesManager.Games)
            {
                dataGridView_Videogames.Rows.Add(g.Name, g.ReviewsAmount, g.AverageScore, "Editar", "Borrar");
            }
        }
        private void button_newGame_Click(object sender, EventArgs e)
        {
            try
            {
                string name = "";
                name = Interaction.InputBox("Ingrese el Nombre del Juego: ", "Nuevo Juego");
                if (name == "")
                {
                    throw new Exception("El nombre ingresado es invalido!");
                }
                gamesManager.GameCreate(new Game(name));
                UpdateGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView_Videogames_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0) return;
            try
            {
                switch (e.ColumnIndex)
                {
                    case 3:
                        string currentName = dataGridView_Videogames[0, e.RowIndex].Value.ToString();
                        string name = Interaction.InputBox("Ingrese el Nombre del Juego: ", "Editar Juego", $"{currentName}");
                        if (String.IsNullOrWhiteSpace(name))
                        {
                            throw new Exception("El nombre ingresado es invalido!");
                        }
                        if (currentName != name)
                        {
                            gamesManager.GameModify(new Game(gamesManager.Games[e.RowIndex].Id, name, gamesManager.Games[e.RowIndex].ReviewsAmount, gamesManager.Games[e.RowIndex].AverageScore));
                            UpdateGrid();
                        }
                        break;
                    case 4:
                        var response = Interaction.MsgBox($"Desea eliminar el juego '{dataGridView_Videogames[0, e.RowIndex].Value}' y todas sus reseñas?", MsgBoxStyle.YesNo, "Eliminar Juego");
                        if (response == MsgBoxResult.Yes) { gamesManager.GameDelete(gamesManager.Games[e.RowIndex].Id); }
                        UpdateGrid();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
