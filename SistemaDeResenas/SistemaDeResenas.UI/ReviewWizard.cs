﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaDeResenas.BE;
using SistemaDeResenas.BLL;

namespace SistemaDeResenas.UI
{
    public partial class ReviewWizard : Form
    {
        GamesManager gamesManager;
        ReviewManager reviewsManager;
        User user;
        List<Review> userReviews;
        Users userForm;
        bool edition;
        bool isDataSaved = false;
        string closeModalString;
        Game selectedVideogame;
        Review selectedReview;
        int score;


        public ReviewWizard(bool edition, User user)
        {
            InitializeComponent();
            this.edition = edition;
            this.user = user;
            gamesManager = new GamesManager();
        }

        private void Reviews_Load(object sender, EventArgs e)
        {
            reviewsManager = new ReviewManager();
            userForm = (Users)Owner.ActiveMdiChild;
            userReviews = reviewsManager.ReviewsList(user.Id);
            SetWizardMode();
        }

        private void SetWizardMode()
        {
            if (!edition)
            {
                gamesManager.GamesList();
                Text = "Agregar Reseña";
                button_Save.Text = "Crear";
                closeModalString = "¿Desea descartar la creacion de la reseña?";
                foreach (Game g in gamesManager.Games)
                {
                    comboBox_Videogames.Items.Add(g);
                }
                button_Save.Click += button_Save_ClickCREATE;
            }
            else
            {
                Text = "Editar Reseñas";
                button_Save.Text = "Guardar";
                closeModalString = "¿Desea descartar los cambios en la reseña?";
                foreach (Review r in userReviews)
                {
                    comboBox_Videogames.Items.Add(r.Videogame);
                }
                button_Save.Click += button_Save_ClickCREATE;
                button_Save.Enabled = false;
            }
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ReviewWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isDataSaved)
            {
                DialogResult result = MessageBox.Show(closeModalString, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No) { e.Cancel = true; }
            }
        }

        private void radioButton_Click(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                score = Int32.Parse(rb.Text);
            }
        }

        private void comboBox_Videogames_SelectedIndexChanged(object sender, EventArgs e)
        {
            button_Save.Enabled = (!(comboBox_Videogames.SelectedItem == null)) ? true : false;
            selectedVideogame = (Game)comboBox_Videogames.SelectedItem;
            if (edition)
            {
                UpdateFields((Game)comboBox_Videogames.SelectedItem);
            }
        }
        private void UpdateFields(Game game)
        {
            button_Save.Enabled = true;
            foreach (Review r in userReviews)
            {
                if (r.Videogame == game)
                {
                    selectedReview = r;
                    foreach (RadioButton rb in panel_Radiobuttons.Controls)
                    {
                        if (rb.Text == r.Score.ToString())
                        {
                            rb.Checked = true;
                            score = Int32.Parse(rb.Text);
                        }
                        else
                        {
                            rb.Checked = false;
                        }
                    }
                    textBox_Comment.Text = r.Message;
                }
            }
        }

        private void button_Save_ClickCREATE(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_Videogames.SelectedItem == null) { throw new Exception("El videojuego seleccionado es inválido"); }
                bool radioButtonIsSelected = false;
                foreach (RadioButton rb in panel_Radiobuttons.Controls) { if (rb.Checked == true) radioButtonIsSelected = true; }
                if (!radioButtonIsSelected) { throw new Exception("Seleccione un puntaje para el videojuego."); }
                foreach (Review r in userReviews)
                {
                    if (r.Videogame.ToString() == selectedVideogame.ToString())
                    {
                        edition = true;
                        selectedReview = r;
                        button_Save_ClickEDIT(sender, e); return;
                    }
                }
                Review review = new BE.Review(0, user, selectedVideogame, textBox_Comment.Text, (byte)score);
                userForm.ReviewCreate(review);
                isDataSaved = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void button_Save_ClickEDIT(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_Videogames.SelectedItem == null) { throw new Exception("El videojuego seleccionado es inválido"); }
                bool radioButtonIsSelected = false;
                foreach (RadioButton rb in panel_Radiobuttons.Controls) { if (rb.Checked == true) radioButtonIsSelected = true; }
                if (!radioButtonIsSelected) { throw new Exception("Seleccione un puntaje para el videojuego."); }
                Review review = new BE.Review(selectedReview.Id, user, selectedVideogame, textBox_Comment.Text, (byte)score);
                reviewsManager.ReviewModify(review);
                isDataSaved = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
