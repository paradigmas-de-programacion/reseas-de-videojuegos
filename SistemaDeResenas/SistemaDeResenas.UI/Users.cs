﻿using Microsoft.VisualBasic;
using SistemaDeResenas.BE;
using SistemaDeResenas.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaDeResenas.UI
{
    public partial class Users : Form
    {
        ToolStripLabel label;
        UsersManager usersManager;
        ReviewManager reviewManager;

        public Users(ToolStripLabel label)
        {
            InitializeComponent();
            this.label = label;
            label.Enabled = false;

            usersManager = new UsersManager();
            reviewManager = new ReviewManager();
        }
        private void Users_FormClosed(object sender, FormClosedEventArgs e)
        {
            label.Enabled = true;
        }

        private void Users_Load(object sender, EventArgs e)
        {
            UpdateGrid();
        }
        public void UpdateGrid()
        {
            usersManager.UsersList();
            dataGridView_Users.Rows.Clear();
            foreach (User u in usersManager.Users)
            {
                dataGridView_Users.Rows.Add(u.FirstNm, u.LastNm, u.BirthdateCStoGrid(), u.Nickname, "Editar", "Borrar", "Nueva Reseña", "Ver Reseñas");
            }
        }

        private void button_newUser_Click(object sender, EventArgs e)
        {
            try
            {
                Form userWizard = new UserWizard(false, new User());
                userWizard.ShowDialog(this.Parent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView_Users_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0) return;
            try
            {
                switch (e.ColumnIndex)
                {
                    case 4:
                        EditButtonClick(e);
                        break;
                    case 5:
                        DeleteButtonClick(e);
                        break;
                    case 6:
                        AddReviewButtonClick(e);
                        break;
                    case 7:
                        ViewReviewsButtonClick(e);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EditButtonClick(DataGridViewCellEventArgs e)
        {
            Form userWizard = new UserWizard(true, usersManager.Users[e.RowIndex]);
            userWizard.ShowDialog(this.Parent);
        }
        private void DeleteButtonClick(DataGridViewCellEventArgs e)
        {
            DialogResult result = MessageBox.Show($"¿Seguro que desea eliminar el usuario de: {usersManager.Users[e.RowIndex].Nickname}?", "Eliminar Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                usersManager.UserDelete(usersManager.Users[e.RowIndex]);
                UpdateGrid();
            }
        }
        private void AddReviewButtonClick(DataGridViewCellEventArgs e)
        {
            Form reviewWizard = new ReviewWizard(false, usersManager.Users[e.RowIndex]);
            reviewWizard.ShowDialog(this.Parent);
        }
        private void ViewReviewsButtonClick(DataGridViewCellEventArgs e)
        {
            Form reviewWizard = new ReviewWizard(true, usersManager.Users[e.RowIndex]);
            reviewWizard.ShowDialog(this.Parent);
        }


        public void UserCreate(User user)
        {
            usersManager.UserCreate(user);
        }
        public void UserModify(User user)
        {
            usersManager.UserModify(user);
        }
        public void ReviewCreate(Review review)
        {
            reviewManager.ReviewCreate(review);
        }
    }
}