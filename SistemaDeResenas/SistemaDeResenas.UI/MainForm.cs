﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaDeResenas.UI
{
    public partial class MainForm : Form
    {
        Form activeForm;

        public MainForm()
        {
            InitializeComponent();
        }

        //Open & Close Forms
        private void toolStripLabel_Users_Click(object sender, EventArgs e)
        {
            activeForm = new Users(toolStripLabel_Users);
            OpenActiveForm();
        }
        private void toolStripLabel_Videogames_Click(object sender, EventArgs e)
        {
            activeForm = new Videogames(toolStripLabel_Videogames);
            OpenActiveForm();
        }

        private void OpenActiveForm()
        {
            activeForm.MdiParent = this;
            activeForm.WindowState = FormWindowState.Maximized;
            activeForm.Show();
            CloseInactiveForms();
        }
        private void CloseInactiveForms()
        {
            if (MdiChildren.Count() < 1) return;
            foreach (Form f in this.MdiChildren)
            {
                if (f != activeForm) { CloseForm(f); }
            }
        }
        private void CloseForm(Form f)
        {
            f.Close();
        }
    }
}
