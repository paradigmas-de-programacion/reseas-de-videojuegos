﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaDeResenas.BE
{
    public class Game
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int ReviewsAmount { get; private set; }
        public float AverageScore { get; private set; }

        public Game() { }
        public Game(string name)
        {
            Name = name;
        }
        public Game(int id, string name, int reviewsamount, float averagescore)
        {
            Id = id;
            Name = name;
            ReviewsAmount = reviewsamount;
            AverageScore = averagescore;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
