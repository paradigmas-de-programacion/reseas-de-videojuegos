﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaDeResenas.BE
{
    public class User
    {
        public int Id { get; private set; }
        public string FirstNm { get; private set; }
        public string LastNm { get; private set; }
        public DateTime Birthdate { get; private set; }
        public string Nickname {  get; private set; }

        public User() { }
        public User(string firstNm, string lastNm, string birthdate, string nickname)
        {
            FirstNm = firstNm;
            LastNm = lastNm;
            Nickname = nickname;

            Birthdate = BirthdateSQLtoCS(birthdate);
        }
        public User(int id, string firstNm, string lastNm, string birthdate, string nickname)
        {
            Id = id;
            FirstNm = firstNm;
            LastNm = lastNm;
            Nickname = nickname;

            Birthdate = BirthdateSQLtoCS(birthdate);
        }

        private DateTime BirthdateSQLtoCS(string dttm)
        {
            return Birthdate = DateTime.Parse(dttm);
        }
        public string BirthdateCStoSQL()
        {
            return Birthdate.ToString("yyyy-MM-dd");
        }
        public string BirthdateCStoGrid()
        {
            string dttm = $"{Birthdate.Day}-{Birthdate.Month}-{Birthdate.Year}";
            return dttm;
        }
    }
}
