﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaDeResenas.BE
{
    public class Review
    {
        public int Id { get; private set; }
        public User User { get; private set; }
        public Game Videogame { get; private set; }
        public string Message { get; private set; }
        public byte Score { get; private set; }

        public Review(User user, Game videogame, string message, byte score)
        {
            User = user;
            Videogame = videogame;
            Message = message;
            Score = score;
        }
        public Review(int id, User user, Game videogame, string message, byte score)
        {
            Id = id;
            User = user;
            Videogame = videogame;
            Message = message;
            Score = score;
        }
    }
}
