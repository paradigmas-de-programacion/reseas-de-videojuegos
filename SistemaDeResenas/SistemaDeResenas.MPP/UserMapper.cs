﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SistemaDeResenas.BE;

namespace SistemaDeResenas.MPP
{
    public class UserMapper : Mapper
    {
        private string CreateString { get; } = "UserCreate";
        private string ModifyString { get; } = "UserModify";
        private string DeleteString { get; } = "UserDelete";
        private string ListString { get; } = "UsersList";

        public void UserCreate(User user)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@FirstName", SqlDbType = SqlDbType.NVarChar, Value = user.FirstNm},
                new SqlParameter() { ParameterName = "@LastName", SqlDbType = SqlDbType.NVarChar, Value = user.LastNm},
                new SqlParameter() { ParameterName = "@BDate", SqlDbType = SqlDbType.Date, Value = user.BirthdateCStoSQL()},
                new SqlParameter() { ParameterName = "@Nick", SqlDbType = SqlDbType.NVarChar, Value = user.Nickname}
            };
            Write(CreateString, parameters);
        }
        public void UserModify(User user)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = user.Id},
                new SqlParameter() { ParameterName = "@FirstName", SqlDbType = SqlDbType.NVarChar, Value = user.FirstNm},
                new SqlParameter() { ParameterName = "@LastName", SqlDbType = SqlDbType.NVarChar, Value = user.LastNm},
                new SqlParameter() { ParameterName = "@BDate", SqlDbType = SqlDbType.Date, Value = user.BirthdateCStoSQL()},
                new SqlParameter() { ParameterName = "@Nick", SqlDbType = SqlDbType.NVarChar, Value = user.Nickname}
            };
            Write(ModifyString, parameters);
        }
        public void UserDelete(User user)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = user.Id}
            };
            Write(DeleteString, parameters);
        }
        public List<User> UsersList()
        {
            DataTable table = Read(ListString);
            List<User> usersList = new List<User>();
            foreach (DataRow row in table.Rows)
            {
                User user= new User((Int32)(row["Id"]), row["FirstNm"].ToString(), row["LastNm"].ToString(), row["Birthdate"].ToString(), row["Nickname"].ToString());
                usersList.Add(user);
            }
            return usersList;
        }
    }
}
