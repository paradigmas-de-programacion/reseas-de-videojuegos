﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using SistemaDeResenas.DB;

namespace SistemaDeResenas.MPP
{
    public abstract class Mapper
    {
        ConnectionHandler db;
        SqlCommand cmd;

        private static string CreateString { get; }
        private static string DeleteString { get; }

        protected void Write(string procedureName, List<SqlParameter> parameters)
        {
            using (db = new ConnectionHandler())
            {
                using (cmd = new SqlCommand(procedureName))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (var parameter in parameters) { cmd.Parameters.Add(parameter); }
                        cmd.Connection = db.Connect();
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException ex) { throw ex; }
                    finally { db.Disconnect(); }
                }
            }
        }

        protected DataTable Read(string procedureName)
        {
            DataTable table = new DataTable();
            Hashtable ht = new Hashtable();

            using (db = new ConnectionHandler())
            {
                using (cmd = new SqlCommand(procedureName))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = db.Connect();
                        cmd = new SqlCommand(procedureName, db.conn);

                        if (ht != null)
                        {
                            foreach (string s in ht.Keys)
                            {
                                cmd.Parameters.AddWithValue(s, ht[s]);
                            }
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(table);
                        }
                    }
                    catch (SqlException ex) { throw ex; }
                    finally { db.Disconnect(); }
                }
            }
            return table;
        }
        protected DataTable Read(string procedureName, List<SqlParameter> parameters)
        {
            DataTable table = new DataTable();
            Hashtable ht = new Hashtable();

            using (db = new ConnectionHandler())
            {
                using (cmd = new SqlCommand(procedureName, db.conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (var parameter in parameters) { cmd.Parameters.Add(parameter); }
                        cmd.Connection = db.Connect();
                        cmd.ExecuteNonQuery();

                        if (ht != null)
                        {
                            foreach (string s in ht.Keys)
                            {
                                cmd.Parameters.AddWithValue(s, ht[s]);
                            }
                        }
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(table);
                    }
                    catch (SqlException ex) { throw ex; }
                    finally { db.Disconnect(); }
                }
            }
            return table;
        }
    }
}