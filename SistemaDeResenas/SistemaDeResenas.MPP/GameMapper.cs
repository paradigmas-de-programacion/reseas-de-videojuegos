﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SistemaDeResenas.BE;

namespace SistemaDeResenas.MPP
{
    public class GameMapper : Mapper
    {
        private string CreateString { get; } = "GameCreate";
        private string ModifyString { get; } = "GameModify";
        private string DeleteString { get; } = "GameDelete";
        private string ListString { get; } = "GamesList";
        private string ListTopString { get; } = "GamesListTop";

        public void GameCreate(Game game)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = game.Name}
            };
            Write(CreateString, parameters);
        }
        public void GameModify(Game game)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = game.Id},
                new SqlParameter() { ParameterName = "@Name", SqlDbType = SqlDbType.NVarChar, Value = game.Name}
            };
            Write(ModifyString, parameters);
        }
        public void GameDelete(int id)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            Write(DeleteString, parameters);
        }
        public List<Game> GamesList(bool top)
        {
            DataTable table = !top ? Read(ListString) : Read(ListTopString);
            List<Game> gamesList = new List<Game>();
            foreach (DataRow row in table.Rows)
            {
                gamesList.Add(new Game(Int32.Parse(row["Id"].ToString()), row["Name"].ToString(), Int32.Parse(row["ReviewsAmount"].ToString()), (float.TryParse(row["AverageScore"].ToString(), out float result)) ? result : 0f)); 
            }
            return gamesList;
        }
    }
}