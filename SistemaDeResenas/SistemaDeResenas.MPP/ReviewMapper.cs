﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using SistemaDeResenas.BE;

namespace SistemaDeResenas.MPP
{
    public class ReviewMapper : Mapper
    {
        private string CreateString { get; } = "ReviewCreate";
        private string ModifyString { get; } = "ReviewModify";
        private string ListString { get; } = "ReviewList";
        private string DeleteString { get; } = "ReviewDelete";

        public void ReviewCreate(Review review)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@UserId", SqlDbType = SqlDbType.Int, Value = review.User.Id},
                new SqlParameter() { ParameterName = "@VideogameId", SqlDbType = SqlDbType.Int, Value = review.Videogame.Id},
                new SqlParameter() { ParameterName = "@Message", SqlDbType = SqlDbType.NVarChar, Value = review.Message},
                new SqlParameter() { ParameterName = "@Score", SqlDbType = SqlDbType.Int, Value = review.Score}
            };
            Write(CreateString, parameters);
        }
        public void ReviewModify(Review review)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = review.Id},
                new SqlParameter() { ParameterName = "@Message", SqlDbType = SqlDbType.NVarChar, Value = review.Message},
                new SqlParameter() { ParameterName = "@Score", SqlDbType = SqlDbType.Int, Value = review.Score}

            };
            Write(ModifyString, parameters);
        }
        public List<Review> ReviewList(int userId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@UserId", SqlDbType = SqlDbType.Int, Value = userId}
            };
            DataTable table = Read(ListString, parameters);
            List<Review> reviewsList = new List<Review>();
            foreach (DataRow row in table.Rows)
            {
                User user = new User((Int32)(row["UserId"]), row["FirstNm"].ToString(), row["LastNm"].ToString(), row["Birthdate"].ToString(), row["Nickname"].ToString());
                Game game = new Game(Int32.Parse(row["VideogameId"].ToString()), row["Name"].ToString(), Int32.Parse(row["ReviewsAmount"].ToString()), (float.TryParse(row["AverageScore"].ToString(), out float result)) ? result : 0f);
                reviewsList.Add(new Review(Int32.Parse(row["Id"].ToString()), user, game, row["Message"].ToString(), Byte.Parse(row["Score"].ToString())));
            }
            return reviewsList;
        }
        public void ReviewDelete(Review review)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = review.Id}
            };
            Write(DeleteString, parameters);
        }
    }
}
