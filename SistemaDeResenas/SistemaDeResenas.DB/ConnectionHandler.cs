﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaDeResenas.DB
{
    public class ConnectionHandler : IDisposable
    {
        public SqlConnection conn { get; private set; }
        private string connString { get; }

        public ConnectionHandler()
        {
            connString = "Data Source=(LocalDB)\\MSSQLLocalDB;Initial Catalog=SistemaDeResenas;Integrated Security=True";
        }
        public SqlConnection Connect()
        {
            conn = new SqlConnection(connString);
            conn.Open();
            return conn;
        }

        public void Disconnect()
        {
            conn.Close();
            conn.Dispose();
            conn = null;
            GC.Collect();
        }

        public void Dispose()
        {
            
        }
    }
}
