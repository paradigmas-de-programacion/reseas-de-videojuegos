﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaDeResenas.BE;
using SistemaDeResenas.MPP;

namespace SistemaDeResenas.BLL
{
    public class ReviewManager
    {
        private ReviewMapper Mapper { get; set; }

        public ReviewManager()
        {
            Mapper = new ReviewMapper();
        }

        public void ReviewCreate(Review review)
        {
            Mapper.ReviewCreate(review);
        }
        public void ReviewModify(Review review)
        {
            Mapper.ReviewModify(review);
        }
        public List<Review> ReviewsList(int id)
        {
            return Mapper.ReviewList(id);
        }
        public void ReviewDelete(Review review)
        {
            Mapper.ReviewDelete(review);
        }
    }
}
