﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaDeResenas.BE;
using SistemaDeResenas.MPP;

namespace SistemaDeResenas.BLL
{
    public class GamesManager
    {
        private GameMapper Mapper { get; set; }
        public List<Game> Games { get; private set; }
        public List<Game> TopGames { get; private set; }

        public GamesManager()
        {
            Mapper = new GameMapper();
        }

        public void GameCreate(Game game)
        {
            Mapper.GameCreate(game);
        }
        public void GameModify(Game game)
        {
            Mapper.GameModify(game);
        }
        public void GameDelete(int id)
        {
            Mapper.GameDelete(id);
        }
        public void GamesList()
        {
            Games = Mapper.GamesList(false);
        }
        public void GamesTopList()
        {
            Games = Mapper.GamesList(true);
        }
    }
}
