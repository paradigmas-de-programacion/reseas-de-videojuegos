﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaDeResenas.BE;
using SistemaDeResenas.MPP;

namespace SistemaDeResenas.BLL
{
    public class UsersManager
    {
        private UserMapper Mapper { get; set; }
        public List<User> Users { get; private set; }

        public UsersManager()
        {
            Mapper = new UserMapper();
            UsersList();
        }

        public void UserCreate(User user)
        {
            Mapper.UserCreate(user);
        }
        public void UserModify(User user)
        {
            Mapper.UserModify(user);
        }
        public void UserDelete(User user)
        {
            Mapper.UserDelete(user);
        }
        public void UsersList()
        {
            Users = Mapper.UsersList();
        }
    }
}
